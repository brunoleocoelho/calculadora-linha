#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<C:\Program Files (x86)\CodeBlocks\MinGW\include\conio21\conio.c>

/**ATUALIZA��ES DO C�DIGO:
Este c�digo � uma calculadora simples executada no prompt.
2015-Mar:   Vers�o 1.0 - Cria��o de Calculadora Simples 1.0 com soma, subtra��o, multiplica��o e divis�o.
2015-Abr:   Vers�o 2.0 - Criadas fun��es procedurais para cada operacao aritim�tica, e para outras tr�s
            funcionalidades: Porcentagem, Potencia e Resto da divis�o.
            Inclus�o da biblioteca string.h para uso das fun��es strcomp()
            Inclus�o da biblioteca <conio.c> para colorir textos e fundo de tela.
2015-Abr-15 Update 2.1 - Adicionada opera��o de Fatorial, e Percentual.
2015-Mai-11 Adicionado opea��o MDC.
            Invertida a ordem dos pedidos ao usu�rio: Opera��o, 1� numero e 2� numero
            Criadas uma fun��o para cada pedido de n�mero
            Fun��o Fatrorial foi melhorada
2015-Jul-29 Adicionada fun��o de ra�z

*/

/**VARIAVEIS GLOBAIS*/
float n1,n2;//VARIAVEIS DOS NUMEROS ATRIBUIDOS PELO USUARIO E QUE SERAO CALCULADOS.

/**PROTOTIPOS DAS FUNCOES*/
float NUM1(float x);
float NUM2(float y);
float Percentual();
float Soma();
float Subtracao();
float Multiplicacao();
float Divisao();
float Potencia();
int Resto();
int Fatorial(int x);
int MDC ( int D, int d );
int raiz(float num);


void main(void){
    system("color f1");//ALTERA O ESQUEMA DE CORES DO CONSOLE
    system("title CALCULADORA ARITIM�TICA VERSION 2.1");//TEXTO QUE APARECE NA BARRA DE TITULO DA JANELA

    int dec=0;//vari�vel para decis�o final se deseja ou n�o nova opera��o.
    char op;//vari�vel da opera��o desejada.

    do{
        printf("------------------------------------------------------------\n\n");
        printf("           CALCULADORA ARITIM%cTICA VERSION 2.1",144);
        printf("\n            Developed in 2015 by Bruno Coelho\n\n");
        printf("------------------------------------------------------------\n");

        //Exibem a hora que o programa foi executada//DESATIVEI POR MINHA PR�RPIA OP��O
        //printf ("\tData: %s Hora: %s\n",__DATE__,__TIME__);
        //("date /t");
        //system("time /t");

            loop1://Le e verifica se � uma opera��o v�lida, atribuindo em "op"
                printf("\n\tDigite o c%cdigo da opera%c%co que ir%c realizar: ", 162, 135, 132,160);
                printf("\n\tSOMA:\t\t+\t\tSUBTRACAO:\t-\n\tMULTIPLICACAO:\t*\t\tDIVISAO:\t/");
                printf("\n\tPOTENCIA:\t^\t\tPORCENTAGEM:\t%c\n\tRESTO DIVISAO:\tr\t\tRAIZ \t\tz", 37);
                printf("\n\tFATORIAL \t!\t\tPERCENTUAL \tp\n\tMDC  \t\tm\n\t  Operacao: ");
                    scanf("%s",&op);

        switch(op){ //esta 'switch' verifica a opera��o digitada e a associa � fun��o correta.
                case '+':
                    NUM1(n1);
                    NUM2(n2);
                    Soma();
                    break;
                case '-':
                    NUM1(n1);
                    NUM2(n2);
                    Subtracao();
                    break;
                case '*':
                    NUM1(n1);
                    NUM2(n2);
                    Multiplicacao();
                    break;
                case '/':
                    NUM1(n1);
                    NUM2(n2);
                    Divisao();
                    break;
                case '%':
                    NUM1(n1);
                    NUM2(n2);
                    Porcentagem();
                    break;
                case '^':
                    NUM1(n1);
                    NUM2(n2);
                    Potencia();
                    break;
                case 'r':
                    NUM1(n1);
                    NUM2(n2);
                    Resto();
                    break;
                case '!':
                    NUM1(n1);
                    if (n1<0) {
                        textbackground(WHITE);
                        textcolor(LIGHTRED);
                        printf("\tNumero invalido para Fatorial");
                        textbackground(WHITE);
                        textcolor(BLUE);
                    }else Fatorial(n1);
                    break;
                case 'p':
                    NUM1(n1);
                    NUM2(n2);
                    Percentual();
                    break;
                case 'm':
                    NUM1(n1);
                    NUM2(n2);
                    MDC( n1, n2 );
                    break;
                case 'z':
                	NUM1(n1);
                	raiz(n1);
                	break;
                default://Mensagem que aparece se n�o for digitada uma opera��o v�lida.
                    textbackground(WHITE);
                    textcolor(LIGHTRED);
                    printf("\n\tERRO! OPERACAO INVALIDA!\n");
                    textbackground(WHITE);
                    textcolor(BLUE);
                    goto loop1;//volta a pedir uma opera��o
            }

       textbackground(WHITE);
       textcolor(BLUE);
       printf("\n\nDeseja realizar uma nova operacao?\n Digite 1-SIM\n Digite 2-NAO\nResposta: ");
            scanf("%i",&dec);
            system("cls");
       }while (dec!=2);

    printf ("\n\n-----THANK YOU FOR USING MY CALCULATOR----\n\t\t--SEE YOU NEXT!--\n\n");
    system("pause>nul");
}   //FIM DO MAIN

float NUM1(float x){
            //loop2://Le e verifica se � um n�mero v�lido, atribuindo em "n1" //STAND BY
                printf ("\n\tDigite algum n%cmero: ",163);
                scanf ("%f",&n1);
}

float NUM2(float y){
            //loop3://Le e verifica se � um n�mero v�lido, atribuindo em "n2" //STAND BY
                printf ("\n\tDigite outro n%cmero: ",163);
                scanf ("%f",&n2);
}

float Percentual(){
    textbackground(WHITE);
        if (n2==0){//caso "n2" seja ZERO, aparece mensagem.
            textcolor(LIGHTRED);
            printf("\n\tNAO EXISTE PORCENTAGEM DE ZERO\n");
        }else{
            textcolor(GREEN);
            printf("\n\tCalculo do Percentual do primeiro numero em rela%c%co ao segundo.",135, 132);
            printf("\n\tResultado: %.2f equivale a %.2f%c de %.2f\n",n1,(n1/n2)*100,37,n2);
        }
}

float Soma(){
    textbackground(WHITE);
    textcolor(GREEN);
    printf("\n\tResultado da %.2f + %.2f = %.2f\n",n1, n2, n1+n2);
}

float Subtracao(){
    textbackground(WHITE);
    textcolor(GREEN);
    printf("\n\tResultado de %.2f - %.2f = %.2f\n",n1, n2, n1-n2);
}

float Multiplicacao(){
    textbackground(WHITE);
    textcolor(GREEN);
    printf("\n\tResultado de %.2f x %.2f = %.2f\n",n1, n2, n1*n2);
}

float Divisao(){
    textbackground(WHITE);
    if (n2==0){//caso opera��o seja "/" e "n2" seja ZERO, aparece mensagem.
        textcolor(LIGHTRED);
        printf("\n\tNAO EXISTE DIVISAO POR DIVISOR ZERO\n");
    }else{
        textcolor(GREEN);
        printf("\n\tResultado de %.2f %c %.2f = %.4f\n",n1, 246, n2, n1/n2);
    }
}

float Potencia(){
    float expoente;
    float resultado;
    resultado=n1;
    for(expoente=1;expoente<n2;expoente++){
        resultado*=n1;
    }
    textbackground(WHITE);
    textcolor(GREEN);
    printf("\n\tO numero %.2f elevado a %.2f eh igual a %.f.\n",n1,n2,resultado);
}

int Resto(int num1, int num2){
    num1=n1;
    num2=n2;
    textbackground(WHITE);
    if (n2>n1||n2==0){//caso opera��o seja "/" e "n2" seja ZERO, aparece mensagem.
        textcolor(LIGHTRED);
        printf("\n\tPARA OBTER RESTO, O DIVISOR NAO PODE SER ZERO NEM MAIOR QUE O DIVIDENDO\n");
    }else{
        textcolor(GREEN);
        printf("\n\tO Resto da divisao de %d por %d = %d\n", num1, num2, num1%num2);
    }
}

int Fatorial(int x){
    int aux;
    long int fat=1;
    //printf("Valor de x: ");
    //scanf("%d",&x);
    for(aux=1 ;aux<=x; aux++){
        fat = fat*aux;
        //printf("fat = %d\n", fat);//linha teste
    }
    textbackground(WHITE);
    textcolor(GREEN);
    printf("\n\tO fatorial de %.0f %c: %d",n1,130,fat);
    return fat;
}

int Porcentagem(){
    textbackground(WHITE);
    if (n2==0){//caso "n2" seja ZERO, aparece mensagem.
        textcolor(LIGHTRED);
        printf("\n\tNAO EXISTE PORCENTAGEM DE ZERO\n");
    }else{
        textcolor(GREEN);
        printf("\n\tCalculo da Porcentagem digitada no primeiro numerno no segundo.");
        printf("\n\tResultado: %.2f%c de %.2f = %.2f", n1, 37, n2, (n1/100)*n2);
    }
}


int MDC ( int D, int d ){ //Fun��o calcula MDC pelo Algoritmo de Euclides.
    int q, r, x, y;
    if (D < d){ //este 'if' verifica qual � o maior numero e p�e em 'D' (dividendo), e o menor em 'd'(divisor)
        x = D;  // vari�vel local 'x' ser� usado para impress�o no final
        y = d;  // vari�vel local 'y' ser� usado para impress�o no final
        D = d;
        d = x;
    }
     do {       //a partir daqui � executado o algoritimo de euclides usando do-while
        q = D/d;
        r = D%d;
        D = d;
        if (r)
        d = r;
        //printf ("q: %d e resto %d\n", q, r); //linha teste de verifica��o das vari�veis
     } while (r>0);
    textbackground(WHITE);
    textcolor(GREEN);
    printf ("\n\tMDC de %d e %d= %0.f", x, y, d);
    //return d;
}

int raiz(float num){
	int i, k, r;
	for (i=1, k=1; num>=k; i++, k+=2){
		num -= k;
		r = i;
	}
	textbackground(WHITE);
    textcolor(GREEN);
    printf("\n\tRaiz de %.0f %c: %d",n1,130,r);
}
