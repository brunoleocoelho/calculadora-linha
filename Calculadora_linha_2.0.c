#include<stdio.h>
#include<stdlib.h>
#include<C:\Program Files (x86)\CodeBlocks\MinGW\include\conio21\conio.c>

/**ATUALIZA��ES DO C�DIGO:
Este c�digo � uma calculadora simples no prompt
2015-Mar:   Vers�o 1.0 Cria��o de Calculadora Simples 1.0 com soma, subtra��o, multiplica��o e divis�o.
2015-Abr:   Vers�o 2.0 Criadas fun��es procedurais para cada operacao aritimetica, e para algumas outras
            funcionalidades: Porcentagem, Potencia e Resto da divis�o.
            Inclus�o da biblioteca string.h para uso das fun��es strcomp()
            Inclus�o da biblioteca <conio.c> para colorir textos e fundo de tela.
            Update 2.1 Adicionada opera��o de Fatorial

*/

/**VARIAVEIS GLOBAIS*/
float n1,n2;//VARIAVEIS DOS NUMEROS ATRIBUIDOS PELO USUARIO E QUE SERAO CALCULADOS.

/**PROTOTIPOS DAS FUNCOES*/
float Porcentagem();
float Soma();
float Subtracao();
float Multiplicacao();
float Divisao();
float Potencia();
int Resto();
int Fatorial();
//int fatorialR();

void main(void){
    system("color f1");//ALTERA O ESQUEMA DE CORES DO CONSOLE
    system("title CALCULADORA ARITIMETICA VERSION 2.0");//TEXTO QUE APARECE NA BARRA DE TITULO DA JANELA

    int dec=0;//vari�vel para decis�o final se deseja ou n�o nova opera��o.
    char op;//vari�vel da opera��o desejada.

    do{
        printf("------------------------------------------------------------\n\n");
        printf("           CALCULADORA ARITIM%cTICA VERSION 2.0",144);
        printf("\n            Developed in 2015 by Bruno Coelho\n\n");
        printf("------------------------------------------------------------\n");

        //Exibem a hora que o programa foi executada//DESATIVEI POR MINHA PR�RPIA OP��O
        //printf ("\tData: %s Hora: %s\n",__DATE__,__TIME__);
        //("date /t");
        //system("time /t");

            //loop1://Le e verifica se � um n�mero v�lido, atribuindo em "n1" //STAND BY
                printf ("\n\tDigite o 1%c n%cmero: ",167,163);
                scanf ("%f",&n1);

            //loop2://Le e verifica se � um n�mero v�lido, atribuindo em "n2" //STAND BY
                printf ("\n\tDigite o 2%c n%cmero: ",167,163);
                scanf ("%f",&n2);

            loop3://Le e verifica se � uma opera��o v�lida, atribuindo em "op"
                printf("\n\tDigite a opera%c%co desejada: ", 135, 132);
                printf("\n\tSOMA:\t\t+\t\tSUBTRACAO:\t-\n\tMULTIPLICACAO:\t*\t\tDIVISAO:\t/");
                printf("\n\tPOTENCIA:\t^\t\tPORCENTAGEM:\t%c\n\tRESTO DIVISAO:\tr");
                printf("\n\tFatorial (1%c n%cmero)\t!\n\t  Operacao: ",167, 163, 37);
                    scanf("%s",&op);

            switch(op){
                case '+':
                    Soma();
                    break;
                case '-':
                    Subtracao();
                    break;
                case '*':
                    Multiplicacao();
                    break;
                case '/':
                    Divisao();
                    break;
                case '%':
                    Porcentagem();
                    break;
                case '^':
                    Potencia();
                    break;
                case 'r':
                    Resto();
                    break;
                case '!':
                    Fatorial();
                    break;
                default://Mensagem que aparece se n�o for digitada uma opera��o v�lida.
                    textbackground(WHITE);
                    textcolor(LIGHTRED);
                    printf("\n\tERRO! OPERACAO INVALIDA!\n");
                    textbackground(WHITE);
                    textcolor(BLUE);
                    goto loop3;//volta a pedir uma opera��o
            }

       textbackground(WHITE);
       textcolor(BLUE);
       printf("\n\nDeseja realizar uma nova operacao?\n Digite 1-SIM\n Digite 2-NAO\nResposta: ");
            scanf("%i",&dec);
            system("cls");
       }while (dec!=2);

    printf ("\n\n-----THANK YOU FOR USING MY CALCULATOR----\n\t\t--SEE YOU NEXT!--\n\n");
    system("pause>nul");
}

float Porcentagem(){
    textbackground(WHITE);
        if (n2==0){//caso "n2" seja ZERO, aparece mensagem.
            textcolor(LIGHTRED);
            printf("\n\tNAO EXISTE PORCENTAGEM DE ZERO\n");
        }else{
            textcolor(GREEN);
            printf("\n\tResultado: %.2f equivale a %.2f%c de %.2f\n",n1,(n1/n2)*100,37,n2);
        }
}

float Soma(){
    textbackground(WHITE);
    textcolor(GREEN);
    printf("\n\tResultado da %.2f + %.2f = %.2f\n",n1, n2, n1+n2);
}

float Subtracao(){
    textbackground(WHITE);
    textcolor(GREEN);
    printf("\n\tResultado de %.2f - %.2f = %.2f\n",n1, n2, n1-n2);
}

float Multiplicacao(){
    textbackground(WHITE);
    textcolor(GREEN);
    printf("\n\tResultado de %.2f x %.2f = %.2f\n",n1, n2, n1*n2);
}

float Divisao(){
    textbackground(WHITE);
    if (n2==0){//caso opera��o seja "/" e "n2" seja ZERO, aparece mensagem.
        textcolor(LIGHTRED);
        printf("\n\tNAO EXISTE DIVISAO POR DIVISOR ZERO\n");
    }else{
        textcolor(GREEN);
        printf("\n\tResultado de %.2f %c %.2f = %.4f\n",n1, 246, n2, n1/n2);
    }
}

float Potencia(){
    float expoente;
    float resultado;
    resultado=n1;
    for(expoente=1;expoente<n2;expoente++){
        resultado*=n1;
    }
    textbackground(WHITE);
    textcolor(GREEN);
    printf("\n\tO numero %.2f elevado a %.2f eh igual a %.f.\n",n1,n2,resultado);
}

int Resto(int num1, int num2){
    num1=n1;
    num2=n2;
    textbackground(WHITE);
    if (n2>n1||n2==0){//caso opera��o seja "/" e "n2" seja ZERO, aparece mensagem.
        textcolor(LIGHTRED);
        printf("\n\tPARA OBTER RESTO, O DIVISOR NAO PODE SER ZERO NEM MAIOR QUE O DIVIDENDO\n");
    }else{
        textcolor(GREEN);
        printf("\n\tO Resto da divisao de %d por %d = %d\n", num1, num2, num1%num2);
    }
}
int Fatorial(){
    int aux, resultado=1;

    //printf("Valor de n1: ");
    //scanf("%d",&n);

    for(aux=1 ;aux!=n1; aux++){
        resultado = resultado*aux;
        //printf("resultado= %d\n", resultado);
    }
    resultado*=n1;
    textbackground(WHITE);
    textcolor(GREEN);
    printf("\n\tO fatorial de %.0f %c: %d",n1,130,resultado);
    return resultado;

}

/*int fatorialR(){
    int r;
    if (n1==1)return (1);
        r=fatorialR(n1-1)*n1;
        return(r);
    printf("resultado= %d", r);
}*/


