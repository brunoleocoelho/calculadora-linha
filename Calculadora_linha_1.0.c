#include<stdio.h>
#include<windows.h>

void main(void){

    int dec=0;//vari�vel para decis�o final se deseja ou n�o nova opera��o.

    printf ("------------------------------------------------------------\n\n");
    printf ("                 CALCULADORA ARITIMETICA\n\n            Developed in 2015 by Bruno Coelho\n\n");
    printf ("------------------------------------------------------------\n\n");

    do{
        char op;//vari�vel da opera��o desejada.
        float num1=0, num2=0;//vari�veis dos n�meros atribu�dos pelo usu�rio e que ser�o calculados.

        loop1://Le e verifica se � um n�mero v�lido, atribuindo em "num1"
            printf ("\n\tDigite o primeiro numero: ");
            scanf ("%f",&num1);

        loop2://Le e verifica se � um n�mero v�lido, atribuindo em "num2"
            printf ("\n\tDigite o segundo numero: ");
            scanf ("%f",&num2);

        loop3://Le e verifica se � uma opera��o v�lida, atribuindo em "op"
            printf("\n\tDigite a operacao desejada (+, -, * ou /): ");
            scanf("%s",&op);

            switch(op){
                case '+':
                    printf("\n\tResultado de %.2f + %.2f = %.2f\n",num1, num2, num1+num2);
                    break;
                case '-':
                    printf("\n\tResultado de %.2f - %.2f = %.2f\n",num1, num2, num1-num2);
                    break;
                case '*':
                    printf("\n\tResultado de %.2f * %.2f = %.2f\n",num1, num2, num1*num2);
                    break;
                case '/':
                    if (num2==0){//caso opera��o seja "/" e "num2" seja ZERO, aparece mensagem.
                        printf("\n\tNAO EXISTE DIVISAO POR ZERO\n");
                    }else{
                        printf("\n\tResultado de %.2f / %.2f = %.4f\n",num1, num2, num1/num2);
                    }
                    break;
                default://Mensagem que aparece se n�o for digitada uma opera��o v�lida.
                    printf("\a\n\tERRO! OPERACAO INVALIDA!\n");
                    goto loop3;//volta a pedir uma opera��o
            }

                printf("\n\nDeseja uma nova operacao?\n Digite 1-SIM\n Digite 2-NAO\nResposta: ");
                    scanf("%i",&dec);
    }while (dec!=2);
    printf ("\n\n-----THANK YOU FOR USING MY SIMPLE CALCULATOR----\n\t\t--SEE YOU NEXT!--\n\n");
}
